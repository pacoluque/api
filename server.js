var express = require('express') // servidor web
var fs = require('fs')
var bodyParser = require('body-parser')
var app = express()
var port = process.env.PORT || 4000
var requestJson = require('request-json') // Cliente web

app.use(bodyParser.json())

app.listen(port)
console.log("API escuchando en el puerto "+port)

var usuarios = require('./usuarios.json')
var credenciales = require('./credenciales.json')
var movimientos = require('./movimientos.json')

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/bdbancofll/collections"
var apiKey= 'apiKey=oaOT9Lvh3bx0flbFuGCT5HfcnvhmUQNQ'
var clienteMLab = null

console.log("Hola mundo")


/********************* V5: GET PARA OBTENER listado de usuarios ******************/
app.get("/apitechu/V5/usuarios", function(req, res)
  {
  clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios?" + apiKey)
  // En body va el json que devuelve el api de MLab
  clienteMLab.get('', function(err, resMLab, body)
    {
    if(!err)
      res.send(body)
    })
  })


  /********************* V5: POST PARA HACER LOGIN ******************/
  app.post("/apitechu/V5/usuarios/login", function(req, res)
    {
    console.log("Haciendo Login")
    var mail = req.headers.mail
    var clave = req.headers.clave

    var query = "&q={'email':'"+mail+"','password':'"+clave+"'}"
    //var query = "&q={'id':"+idCliente+"}&f={'first_name':1,'last_name':1,'_id':0}"

    var respuesta = {"respuesta":"defecto"}

    clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios?" +query + "&l=1&" + apiKey)

    //console.log(urlMLabRaiz + "/usuarios?" +query + "&l=1&" + apiKey)

    // En body va el json que devuelve el api de MLab
    clienteMLab.get('', function(err, resMLab, body)
      {
      if(!err && body.length > 0) // Login OK, y ahora actualizaré el valor en BD
        {
        clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios")
        console.log(clienteMLab)

        var cambio = '{"$set":{"logged":true}}'

        console.log('?q={"id":'+ body[0].id +'}&' +apiKey)

        // El segundo parámetro es el body
        clienteMLab.put('?q={"id":'+ body[0].id +'}&' +apiKey, JSON.parse(cambio), function(errPut, resPut, bodyPut)
          {
          if (!errPut)
            {
            //console.log("Credenciales correctas")
            //respuesta={"clave":"1","valor":"Paco"}
            //console.log(JSON.stringify(respuesta));
            console.log("codigo MLAB: "+resPut.statusCode)
            respuesta = {"Login OK":"Usuario ID "+body[0].id+" - MAIL "+mail+ " logado"}
            res.statusCode=200 ;
            //res.send(respuesta);
            }
          else
            {
            respuesta = {"Login KO":"Usuario " +mail+ " NO logado"}
            res.statusCode = 500;
            }

          console.log(respuesta)
          res.send(respuesta)
          })
        }
      else
        {
        res.statusCode=401;
        respuesta= {"Login KO":"Usuario " +mail+ " no logged"}
        res.send(respuesta)
        }
      })

    console.log("al final del post de mi API respuesta vale:"+JSON.stringify(respuesta))
    })

    /********************* V5: POST PARA HACER logout ******************/
    app.post("/apitechu/V5/usuarios/logout", function(req, res)
      {
      console.log("Haciendo Login")
      var idCliente = req.headers.idcliente

      //var query = "&q={'email':'"+mail+"','password':'"+clave+"'}"
      //var query = "&q={'id':"+idCliente+"}&f={'first_name':1,'last_name':1,'_id':0}"
      var query = "&q={'id':"+idCliente+", 'logged':true}"

      var respuesta = {"respuesta":"defecto"}

      clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios?" +query + "&l=1&" + apiKey)

      //console.log(urlMLabRaiz + "/usuarios?" +query + "&l=1&" + apiKey)

      // En body va el json que devuelve el api de MLab
      clienteMLab.get('', function(err, resMLab, body)
        {
        if(!err && body.length > 0) // el usuario estaba logado
          {
          clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios")
          console.log(clienteMLab)

          var cambio = '{"$set":{"logged":false}}'

          console.log('?q={"id":'+ body[0].id +'}&' +apiKey)

          // El segundo parámetro es el body
          clienteMLab.put('?q={"id":'+ body[0].id +'}&' +apiKey, JSON.parse(cambio), function(errPut, resPut, bodyPut)
            {
            if (!errPut)
              {
              //console.log("Credenciales correctas")
              //respuesta={"clave":"1","valor":"Paco"}
              //console.log(JSON.stringify(respuesta));
              console.log("codigo MLAB: "+resPut.statusCode)
              respuesta = {"LOGOUT OK":"Usuario " +idCliente+ " desconectado"}
              res.statusCode=200 ;
              //res.send(respuesta);
              }
            else
              {
              respuesta = {"LOGOUT KO":"Usuario " +idCliente+ " NO pudo desconectarse"}
              res.statusCode = 500;
              }

            console.log(respuesta)
            res.send(respuesta)
            })
          }
        else
          {
          res.statusCode=200;
          respuesta= {"LOGOUT KO":"Usuario " +idCliente+ " no logado previamente"}
          res.send(respuesta)
          }
        })

      console.log("al final del post de mi API respuesta vale:"+JSON.stringify(respuesta))
      })




  /********************* V5: GET PARA OBTENER o el Recurso
  ********************** usuario o solo el nombre y apellidos de un usuarios dado
  ****************************************************************************************/
  app.get("/apitechu/V5/usuarios/:idCliente", function(req, res)
    {
    var idCliente = req.params.idCliente;
    var query = "&q={'id':"+idCliente+"}"

    // Si nos viene el param filtrar solo devolveremos el nombre y los apellidos
    if (req.query.filtrar == 1)
      query = query + "&q={'id':"+idCliente+"}&f={'first_name':1,'last_name':1,'_id':0}"

    clienteMLab = requestJson.createClient(urlMLabRaiz + "/usuarios?" +query + "&l=1&" + apiKey)

    // En body va el json que devuelve el api de MLab
    clienteMLab.get('', function(err, resMLab, body)
      {
      if(!err && body.length > 0)
        res.send(body[0])
      else
        {
        res.statusCode=404;
        res.send({"error":"Usuario no encontrado"});
        }
      })
    })

  /**************************************************************************************/
  /*********************  GET PARA OBTENER CUENTAS V5 pasado idCliente ******************/
  /**************************************************************************************/
  app.get("/apitechu/V5/cuentas", function(req, res)
    {
    var listaIbanes = [];
    var idCliente = req.query.idcliente;

    var query = "q={'idCliente':"+idCliente+"}"

    clienteMLab = requestJson.createClient(urlMLabRaiz + "/cuentas?" +query + "&" + apiKey)
    console.log("URL de LLAMADA A MLAB "+urlMLabRaiz + "/cuentas?" +query + "&" + apiKey)


    clienteMLab.get('', function(err, resMLab, body)
      {
      console.log("Respuesta MLAB: "+resMLab.statusCode)
      console.log ("num de ibanes encontrados: "+body.length)

      if(!err && body.length > 0)
        {
        for (var i = 0; i < body.length; i++)
          listaIbanes.push({"iban":body[i].iban})

        res.send(listaIbanes)
        }
      else
        {
        res.statusCode=404;
        res.send(listaIbanes)
        }
      })
    })

    /*************************************************************************************/
    /*********************  GET PARA OBTENER MOVIMIENTOS V5 pasado IBAN ******************/
    /*************************************************************************************/

  app.get("/apitechu/V5/cuentas/:iban/movimientos", function(req, res)
    {
    var numIban = req.params.iban;
    var listaMovimientos;

    var query = "q={'iban':'"+numIban+"'}"

    clienteMLab = requestJson.createClient(urlMLabRaiz + "/cuentas?" +query + "&" + apiKey)
    console.log("URL de LLAMADA A MLAB "+urlMLabRaiz + "/cuentas?" +query + "&" + apiKey)


    clienteMLab.get('', function(err, resMLab, body)
      {
      console.log("Respuesta MLAB: "+resMLab.statusCode)
      console.log ("num de cuentas encontrados: "+body.length)

      if(!err && body.length > 0)
        {
        for (var i = 0; i < body.length; i++)
          {
          if (i==0)
            listaMovimientos = body[i].movimientos
          else
            listaMovimientos.push(body[i].movimientos)
          }
        res.send(listaMovimientos)
        }
      else
        {
        res.statusCode=404;
        listaMovimientos = []
        res.send(listaMovimientos)
        }
      })
    })


/*********************************************************************************************************************/



/*********************  GET PARA OBTENER CUENTAS ******************/
app.get("/apitechu/V1/cuentas", function(req, res)
  {
  var listaIbanes = [];
  //var nuevo = {"iban":cuentas[i].iban}

  for (var i = 0; i < movimientos.length; i++)
    listaIbanes.push({"iban":movimientos[i].iban})

  res.send(listaIbanes)
  //res.send({"mensaje":"Bienvenido a mi primera API"})
  })


  /*********************  GET PARA OBTENER MOViMIENTOS DE UNA CUENTA ******************/

  app.get("/apitechu/V1/cuentas/:cc/movimientos", function(req, res)
    {
    var numIban = req.params.cc;
    var listaMovimientos = [];
    //var nuevo = {"iban":cuentas[i].iban}

    for (var i = 0; i < movimientos.length; i++)
      {
      if (numIban == movimientos[i].iban)
        {
        listaMovimientos = movimientos[i].movimientos;
        break;
        }
      }

    res.send(listaMovimientos)
    //res.send({"mensaje":"Bienvenido a mi primera API"})
    })

    /*********************  GET PARA OBTENER CUENTAS DE UN CLIENTE ******************/

    app.get("/apitechu/V1/clientes/:cl/cuentas", function(req, res)
      {
      var numCliente = req.params.cl;
      var listaCuentas = [];
      //var nuevo = {"iban":cuentas[i].iban}

      // Busco las cuentas del cliente dado en todo el fichero
      for (var i = 0; i < movimientos.length; i++)
        {
        if (numCliente == movimientos[i].idCliente)
          {
          listaCuentas.push({"iban":movimientos[i].iban})
          }
        }

      res.send(listaCuentas)
      //res.send({"mensaje":"Bienvenido a mi primera API"})
      })




/********************** PETICION POST PARA HACER LOGIN **************************************************/
app.post('/apitechu/V1', function(req, res)
  {
  var mail = req.headers.mail
  var clave = req.headers.clave
  var respuesta = 'usuario/e-mail no encontrado'

  for (var i = 0; i < credenciales.length; i++)
    {
    if (credenciales[i].email == mail)
      {
      if (credenciales[i].password == clave)
        {
        //console.log('usuario y clave encontrados y correctos')
        respuesta='ID usuario logado: '+credenciales[i].id;
        credenciales[i].logged=true;
        //console.log('la respuesta es '+respuesta)
        break;
        }
      else
        {
        res.statusCode=401
        respuesta='Password incorrecta'
        }
      }
    }
  res.send(respuesta)

  })

  /************************************ LOGOUT **************************************************/
  app.post('/apitechu/V1/logout', function(req, res)
    {
    var idHeader = req.headers.id
    var respuesta = 'usuario/e-mail no encontrado'

    for (var i = 0; i < credenciales.length; i++)
      {
      if (credenciales[i].id == idHeader)
        {
        if (credenciales[i].logged==true)
          {
          //console.log('usuario y clave encontrados y correctos')
          respuesta='Usuario '+credenciales[i].id+': desconectado';
          credenciales[i].logged=false;
          //console.log('la respuesta es '+respuesta)
          break;
          //console.log('La i vale '+i)
          }
        else
          {
          respuesta = 'LOGOUT FAIL - El usuario '+idHeader+' no está conectado';
          }
        }
      }

    console.log('la respuesta antes de devolverla es '+respuesta)
    res.send(respuesta)
    })



/****** PETICION GET *******************/
app.get("/apitechu/V1", function(req, res){
  //console.log(req)
  res.send({"mensaje":"Bienvenido a mi primera API"})
})

/****** PETICION GET *******************/
app.get('/apitechu/V1/usuarios', function(req, res){
  res.send(credenciales)
})

/****** PETICION POST CON HEADERS*******************/
app.post('/apitechu/V1/usuarios', function(req, res){

  var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
  usuarios.push(nuevo);

  const datos = JSON.stringify(usuarios)

  fs.writeFile('./usuarios2.json', datos, 'utf8', function(err){
      if (err)
        console.log(err)
      else
        console.log('Fichero guardado')

        console.log(req.headers)
        res.send("Alta realizada")
    })
  })


  /****** PETICION POST CON BODY*******************/
  app.post('/apitechu/V2/usuarios', function(req, res){

    var usuarioNuevo = req.body
    usuarios.push(usuarioNuevo);

    const datos = JSON.stringify(usuarios)

    fs.writeFile('./usuarios2.json', datos, 'utf8', function(err){
        if (err)
          console.log(err)
        else
          console.log('Fichero guardado')

      res.send("Alta realizada")
      })
    })






/****** PETICION DELETE *******************/
app.delete('/apitechu/V1/usuarios/:id', function(req, res){
  var numUsuario = req.params.id-1
  usuarios.splice(req.params.id-1, 1)

  res.send('Usuario número '+numUsuario+' eliminado con éxito')
  })

/****** PETICION POST mostrar:todo *******************/
app.post('/apitechu/V1/monstruo/:p1/:p2', function(req, res){

  console.log("Parámetros")
  console.log(req.params)
  console.log("Query String")
  console.log(req.query)
  console.log("Headers")
  console.log(req.headers)
  console.log("Body")
  console.log(req.body)

  res.send("Finalizado")

  })
